<?php

    require_once '_configbdd.php';

    try {
        $conn = new mysqli(DB_HOST, DB_USER, DB_PWD, DB_NAME);
        mysqli_set_charset($conn, "utf8");
    }
    catch(Exception $e)
	{
		die('Erreur : '. $e->getMessage());
	}

    $datafeed = $conn->query("SELECT * FROM eleves ORDER BY nomfamille ASC;");

    $eleve_tire = $conn->query("SELECT nomfamille, prenom, noteaddition, passage FROM eleves ORDER BY RAND (NOW()) LIMIT 1;");

    $eleve_tire_null = $conn->query("SELECT nomfamille, prenom FROM eleves WHERE noteaddition IS NULL ORDER BY RAND (NOW()) LIMIT 1;");

?>