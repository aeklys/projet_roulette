<?php

	require_once '_configbdd.php';

	try
	{
		$bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PWD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	}
	catch(Exception $e)
	{
		die('Erreur : '.$e->getMessage());
	}

	$datafeed = "INSERT INTO `eleves` (`nomfamille`, `prenom`, `classe`) VALUES
	('GOUVERNEUR', 'Théo', 'SIO2'),
	('TEIXEIRA', 'Ryan', 'SIO2'),
	('SARAZIN', 'Karen', 'SIO2'),
	('HUREAUX', 'Samuel', 'SIO2'),
	('HANS', 'Matthis', 'SIO2'),
	('COSSE', 'Antonin', 'SIO2'),
	('BONOTTI', 'Luca', 'SIO2'),
	('BOUILLON', 'Bastien', 'SIO2'),
	('SCHMITT', 'Thomas', 'SIO2'),
	('COLLART', 'Thibault', 'SIO2'),
	('LAMBINET', 'Théo', 'SIO2'),
	('SACCO', 'Mattéo', 'SIO2'),
	('DUEZ-HENRY', 'Lucas', 'SIO2'),
	('HUBERT', 'Léa', 'SIO2'),
	('DECHAPPE', 'Gaëtan', 'SIO2'),
	('PASCAL', 'Lendell', 'SIO2')";
?>