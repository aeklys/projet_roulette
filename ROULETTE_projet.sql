CREATE DATABASE IF NOT EXISTS roulette;
USE roulette;

CREATE TABLE IF NOT EXISTS `eleves` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nomfamille` varchar(30) NOT NULL,
    `prenom` varchar(30) NOT NULL,
    `classe` varchar(10) NOT NULL,
    `ldap` tinyint(1) NOT NULL DEFAULT 0,
    `bool` tinyint(1) NOT NULL DEFAULT 0,
    `passage` int(5) NOT NULL DEFAULT 0,
    `absence` tinyint(1) NOT NULL DEFAULT 0,
    `noteaddition` int(100) DEFAULT NULL,
    `notetotal` int(10) DEFAULT NULL,
    `average` int(10) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

INSERT INTO `eleves` (`nomfamille`, `prenom`, `classe`) VALUES
	('GOUVERNEUR', 'Théo', 'SIO2'),
	('TEIXEIRA', 'Ryan', 'SIO2'),
	('SARAZIN', 'Karen', 'SIO2'),
	('HUREAUX', 'Samuel', 'SIO2'),
	('HANS', 'Matthis', 'SIO2'),
	('COSSE', 'Antonin', 'SIO2'),
	('BONOTTI', 'Luca', 'SIO2'),
	('BOUILLON', 'Bastien', 'SIO2'),
	('SCHMITT', 'Thomas', 'SIO2'),
	('COLLART', 'Thibault', 'SIO2'),
	('LAMBINET', 'Théo', 'SIO2'),
	('SACCO', 'Mattéo', 'SIO2'),
	('DUEZ-HENRY', 'Lucas', 'SIO2'),
	('HUBERT', 'Léa', 'SIO2'),
	('DECHAPPE', 'Gaëtan', 'SIO2'),
	('PASCAL', 'Lendell', 'SIO2');